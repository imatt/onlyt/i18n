Thank you for helping to Localise and Translate the [OnlyT Remote](https://onlyt.app) app. 

Please see the [Wiki](https://gitlab.com/imatt/onlyt/i18n/wikis/home) for instructions.